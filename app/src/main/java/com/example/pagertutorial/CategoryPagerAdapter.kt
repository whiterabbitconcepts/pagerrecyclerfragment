package com.example.pagertutorial

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class CategoryPagerAdapter(fragmentManager: FragmentManager, private val categories: ArrayList<Category>) : FragmentStatePagerAdapter(fragmentManager){

    override fun getItem(position: Int): Fragment {
        return CategoryFragment.newInstance(categories.get(position))
    }

    override fun getCount(): Int {
        return categories.size
    }

}