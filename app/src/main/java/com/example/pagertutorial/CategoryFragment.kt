package com.example.pagertutorial

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class CategoryFragment : Fragment() {

    private lateinit var category: Category

    companion object {
        fun newInstance(category: Category) : CategoryFragment {
            val fragment = CategoryFragment()
            fragment.category = category
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate category view
        val root = inflater.inflate(R.layout.category, container, false)

        // Show category label
        val categoryLbl : TextView = root.findViewById(R.id.txtCategory) as TextView
        categoryLbl.text = category.name

        // Setup recycler view for showing the list of products
        val recycler : RecyclerView = root.findViewById(R.id.rcyclrProducts) as RecyclerView

        // Assign layout manager to the recycler view
        val lm = LinearLayoutManager(this.activity)
        lm.orientation = LinearLayoutManager.VERTICAL
        recycler.layoutManager = lm

        // Set the product adapter for this category's products list
        recycler.adapter = ProductAdapter(category.products!!)

        return root
    }
}