package com.example.pagertutorial

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // create the categories array
        val categories: ArrayList<Category> = ArrayList()

        // populate the categories (we'll load two categories)
        categories.add(Category(0, "Electronics", ArrayList()))
        categories.add(Category(1, "Furniture", ArrayList()))

        // add products to the categories
        categories[0].products.add(Product(0, "iPhone X", "Apple 64GB"))
        categories[0].products.add(Product(1, "S9", "Samsung 64GB"))
        categories[1].products.add(Product(0, "Couch", "Grey suede"))
        categories[1].products.add(Product(1, "Table", "Handcrafted oak"))

        // set our category adapter
        val pagerAdapter = CategoryPagerAdapter(supportFragmentManager, categories)
        val categoryPgr = this.pgrCategories as ViewPager
        categoryPgr.adapter = pagerAdapter

    }
}
