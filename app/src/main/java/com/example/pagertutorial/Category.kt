package com.example.pagertutorial

data class Category(val id: Int, val name: String, var products: ArrayList<Product>)