package com.example.pagertutorial

import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView

class ProductAdapter(private var productList: ArrayList<Product>) : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    override fun getItemCount(): Int {
        return productList.size
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product: Product = productList[position]

        holder.name.text = product.name
        holder.description.setText(product.description)

        // add listener for updating the product description
        holder.description.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                product.description = s.toString()
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        var v: View = LayoutInflater.from(parent.context).inflate(R.layout.product, parent, false)
        return ProductViewHolder(v)
    }

    class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name : TextView = itemView.findViewById(R.id.txtProductName)
        var description : EditText = itemView.findViewById(R.id.txtProductDescription)
    }
}