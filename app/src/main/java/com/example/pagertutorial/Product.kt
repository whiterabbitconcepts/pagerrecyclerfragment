package com.example.pagertutorial

data class Product(val id: Int, val name: String, var description: String)